export default {
    init() {
        // JavaScript to be fired on all pages
        jQuery(document).ready(function ($) {
            const mainHeader = $('.cd-auto-hide-header');

            //set scrolling variables
            let scrolling = false,
                previousTop = 0;
            const scrollDelta = 10,
                scrollOffset = 150;

            $(window).on('scroll', function () {
                if (!scrolling) {


                    scrolling = true;
                    (!window.requestAnimationFrame)
                        ? setTimeout(autoHideHeader, 250)
                        : requestAnimationFrame(autoHideHeader);
                }
            });

            function autoHideHeader() {

                const currentTop = $(window).scrollTop();
                checkSimpleNavigation(currentTop);

                previousTop = currentTop;
                scrolling = false;
            }

            function checkSimpleNavigation(currentTop) {
                //there's no secondary nav or secondary nav is below primary nav
                if (previousTop - currentTop > scrollDelta) {
                    //if scrolling up...
                    mainHeader.removeClass('is-hidden');

                } else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
                    //if scrolling down...
                    mainHeader.addClass('is-hidden');
                    $('.open-menu').removeClass('open-menu');
                } else if (currentTop !== 0) {
                    mainHeader.addClass('c-header--is-scrolled');
                } else if (currentTop === 0) {
                    mainHeader.removeClass('c-header--is-scrolled');
                    $('.open-menu').removeClass('open-menu');

                }
            }
        });

        (function () {
            // Back to Top - by CodyHouse.co
            const backTop = document.getElementsByClassName('js-cd-top')[0],
                // browser window scroll (in pixels) after which the "back to top" link is shown
                offset = 300,
                //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
                offsetOpacity = 1200,
                scrollDuration = 700;
            let scrolling = false;
            if (backTop) {
                //update back to top visibility on scrolling
                window.addEventListener('scroll', function () {
                    if (!scrolling) {
                        scrolling = true;
                        (!window.requestAnimationFrame) ? setTimeout(checkBackToTop, 250) : window.requestAnimationFrame(checkBackToTop);
                    }
                });
                //smooth scroll to top
                backTop.addEventListener('click', function (event) {
                    event.preventDefault();
                    (!window.requestAnimationFrame) ? window.scrollTo(0, 0) : scrollTop(scrollDuration);
                });
            }

            function checkBackToTop() {
                const windowTop = window.scrollY || document.documentElement.scrollTop;
                (windowTop > offset) ? addClass(backTop, 'cd-top--show') : removeClass(backTop, 'cd-top--show', 'cd-top--fade-out');
                (windowTop > offsetOpacity) && addClass(backTop, 'cd-top--fade-out');
                scrolling = false;
            }

            function scrollTop(duration) {
                const start = window.scrollY || document.documentElement.scrollTop;
                let currentTime = null;

                const animateScroll = function (timestamp) {
                    if (!currentTime) currentTime = timestamp;
                    const progress = timestamp - currentTime;
                    const val = Math.max(Math.easeInOutQuad(progress, start, -start, duration), 0);
                    window.scrollTo(0, val);
                    if (progress < duration) {
                        window.requestAnimationFrame(animateScroll);
                    }
                };

                window.requestAnimationFrame(animateScroll);
            }

            Math.easeInOutQuad = function (t, b, c, d) {
                t /= d / 2;
                if (t < 1) return c / 2 * t * t + b;
                t--;
                return -c / 2 * (t * (t - 2) - 1) + b;
            };

            //class manipulations - needed if classList is not supported
            function hasClass(el, className) {
                if (el.classList) return el.classList.contains(className);
                else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
            }

            function addClass(el, className) {
                const classList = className.split(' ');
                if (el.classList) el.classList.add(classList[0]);
                else if (!hasClass(el, classList[0])) el.className += ' ' + classList[0];
                if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
            }

            function removeClass(el, className) {
                const classList = className.split(' ');
                if (el.classList) el.classList.remove(classList[0]);
                else if (hasClass(el, classList[0])) {
                    const reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
                    el.className = el.className.replace(reg, ' ');
                }
                if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
            }
        })();

        jQuery(document).ready(function ($) {
            const contentSections = $('.cd-section'),
                navigationItems = $('#cd-vertical-nav a');

            updateNavigation();
            $(window).on('scroll', function () {
                updateNavigation();
            });

            //smooth scroll to the section
            navigationItems.on('click', function (event) {
                event.preventDefault();
                smoothScroll($(this.hash));
            });

            function updateNavigation() {
                contentSections.each(function () {
                    let $this = $(this);
                    const activeSection = $('#cd-vertical-nav a[href="#' + $this.attr('id') + '"]').data('number') - 1;
                    if (($this.offset().top - $(window).height() / 2 < $(window).scrollTop()) && ($this.offset().top + $this.height() - $(window).height() / 2 > $(window).scrollTop())) {
                        navigationItems.eq(activeSection).addClass('is-selected');
                    } else {
                        navigationItems.eq(activeSection).removeClass('is-selected');
                    }
                });
            }

            function smoothScroll(target) {
                $('body,html').animate(
                    {'scrollTop': target.offset().top - 100},
                    600
                );
            }
        });

        function _classCallCheck(t, i) {
            if (!(t instanceof i)) throw new TypeError('Cannot call a class as a function')
        }

        const Sticky = function () {
            function t() {
                const i = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : '',
                    e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                _classCallCheck(this, t), this.selector = i, this.elements = [], this.version = '1.2.0', this.vp = this.getViewportSize(), this.body = document.querySelector('body'), this.options = {
                    wrap: e.wrap || !1,
                    marginTop: e.marginTop || 0,
                    stickyFor: e.stickyFor || 0,
                    stickyClass: e.stickyClass || null,
                    stickyContainer: e.stickyContainer || 'body',
                }, this.updateScrollTopPosition = this.updateScrollTopPosition.bind(this), this.updateScrollTopPosition(), window.addEventListener('load', this.updateScrollTopPosition), window.addEventListener('scroll', this.updateScrollTopPosition), this.run()
            }

            return t.prototype.run = function () {
                const t = this, i = setInterval(function () {
                    if ('complete' === document.readyState) {
                        clearInterval(i);
                        const e = document.querySelectorAll(t.selector);
                        t.forEach(e, function (i) {
                            return t.renderElement(i)
                        })
                    }
                }, 10);
            }, t.prototype.renderElement = function (t) {
                const i = this;
                t.sticky = {}, t.sticky.active = !1, t.sticky.marginTop = parseInt(t.getAttribute('data-margin-top')) || this.options.marginTop, t.sticky.stickyFor = parseInt(t.getAttribute('data-sticky-for')) || this.options.stickyFor, t.sticky.stickyClass = t.getAttribute('data-sticky-class') || this.options.stickyClass, t.sticky.wrap = !!t.hasAttribute('data-sticky-wrap') || this.options.wrap, t.sticky.stickyContainer = this.options.stickyContainer, t.sticky.container = this.getStickyContainer(t), t.sticky.container.rect = this.getRectangle(t.sticky.container), t.sticky.rect = this.getRectangle(t), 'img' === t.tagName.toLowerCase() && (t.onload = function () {
                    return t.sticky.rect = i.getRectangle(t)
                }), t.sticky.wrap && this.wrapElement(t), this.activate(t)
            }, t.prototype.wrapElement = function (t) {
                t.insertAdjacentHTML('beforebegin', '<span></span>'), t.previousSibling.appendChild(t)
            }, t.prototype.activate = function (t) {
                t.sticky.rect.top + t.sticky.rect.height < t.sticky.container.rect.top + t.sticky.container.rect.height && t.sticky.stickyFor < this.vp.width && !t.sticky.active && (t.sticky.active = !0), this.elements.indexOf(t) < 0 && this.elements.push(t), t.sticky.resizeEvent || (this.initResizeEvents(t), t.sticky.resizeEvent = !0), t.sticky.scrollEvent || (this.initScrollEvents(t), t.sticky.scrollEvent = !0), this.setPosition(t)
            }, t.prototype.initResizeEvents = function (t) {
                const i = this;
                t.sticky.resizeListener = function () {
                    return i.onResizeEvents(t)
                }, window.addEventListener('resize', t.sticky.resizeListener)
            }, t.prototype.destroyResizeEvents = function (t) {
                window.removeEventListener('resize', t.sticky.resizeListener)
            }, t.prototype.onResizeEvents = function (t) {
                this.vp = this.getViewportSize(), t.sticky.rect = this.getRectangle(t), t.sticky.container.rect = this.getRectangle(t.sticky.container), t.sticky.rect.top + t.sticky.rect.height < t.sticky.container.rect.top + t.sticky.container.rect.height && t.sticky.stickyFor < this.vp.width && !t.sticky.active ? t.sticky.active = !0 : (t.sticky.rect.top + t.sticky.rect.height >= t.sticky.container.rect.top + t.sticky.container.rect.height || t.sticky.stickyFor >= this.vp.width && t.sticky.active) && (t.sticky.active = !1), this.setPosition(t)
            }, t.prototype.initScrollEvents = function (t) {
                const i = this;
                t.sticky.scrollListener = function () {
                    return i.onScrollEvents(t)
                }, window.addEventListener('scroll', t.sticky.scrollListener)
            }, t.prototype.destroyScrollEvents = function (t) {
                window.removeEventListener('scroll', t.sticky.scrollListener)
            }, t.prototype.onScrollEvents = function (t) {
                t.sticky.active && this.setPosition(t)
            }, t.prototype.setPosition = function (t) {
                this.css(t, {
                    position: '',
                    width: '',
                    top: '',
                    left: '',
                }), this.vp.height < t.sticky.rect.height || !t.sticky.active || (t.sticky.rect.width || (t.sticky.rect = this.getRectangle(t)), t.sticky.wrap && this.css(t.parentNode, {
                    display: 'block',
                    width: t.sticky.rect.width + 'px',
                    height: t.sticky.rect.height + 'px',
                }), 0 === t.sticky.rect.top && t.sticky.container === this.body ? this.css(t, {
                    position: 'fixed',
                    top: t.sticky.rect.top + 'px',
                    left: t.sticky.rect.left + 'px',
                    width: t.sticky.rect.width + 'px',
                }) : this.scrollTop > t.sticky.rect.top - t.sticky.marginTop ? (this.css(t, {
                    position: 'fixed',
                    width: t.sticky.rect.width + 'px',
                    left: t.sticky.rect.left + 'px',
                }), this.scrollTop + t.sticky.rect.height + t.sticky.marginTop > t.sticky.container.rect.top + t.sticky.container.offsetHeight ? (t.sticky.stickyClass && t.classList.remove(t.sticky.stickyClass), this.css(t, {top: t.sticky.container.rect.top + t.sticky.container.offsetHeight - (this.scrollTop + t.sticky.rect.height) + 'px'})) : (t.sticky.stickyClass && t.classList.add(t.sticky.stickyClass), this.css(t, {top: t.sticky.marginTop + 'px'}))) : (t.sticky.stickyClass && t.classList.remove(t.sticky.stickyClass), this.css(t, {
                    position: '',
                    width: '',
                    top: '',
                    left: '',
                }), t.sticky.wrap && this.css(t.parentNode, {display: '', width: '', height: ''})))
            }, t.prototype.update = function () {
                const t = this;
                this.forEach(this.elements, function (i) {
                    i.sticky.rect = t.getRectangle(i), i.sticky.container.rect = t.getRectangle(i.sticky.container), t.activate(i), t.setPosition(i)
                })
            }, t.prototype.destroy = function () {
                const t = this;
                this.forEach(this.elements, function (i) {
                    t.destroyResizeEvents(i), t.destroyScrollEvents(i), delete i.sticky
                })
            }, t.prototype.getStickyContainer = function (t) {
                for (var i = t.parentNode; !i.hasAttribute('data-sticky-container') && !i.parentNode.querySelector(t.sticky.stickyContainer) && i !== this.body;) i = i.parentNode;
                return i
            }, t.prototype.getRectangle = function (t) {
                this.css(t, {position: '', width: '', top: '', left: ''});
                const i = Math.max(t.offsetWidth, t.clientWidth, t.scrollWidth),
                    e = Math.max(t.offsetHeight, t.clientHeight, t.scrollHeight);
                let s = 0, o = 0;
                do s += t.offsetTop || 0, o += t.offsetLeft || 0, t = t.offsetParent; while (t);
                return {top: s, left: o, width: i, height: e}
            }, t.prototype.getViewportSize = function () {
                return {
                    width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
                    height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
                }
            }, t.prototype.updateScrollTopPosition = function () {
                this.scrollTop = (window.pageYOffset || document.scrollTop) - (document.clientTop || 0) || 0
            }, t.prototype.forEach = function (t, i) {
                let e = 0;
                const s = t.length;
                for (; e < s; e++) i(t[e])
            }, t.prototype.css = function (t, i) {
                for (let e in i) i.hasOwnProperty(e) && (t.style[e] = i[e])
            }, t
        }();
        !function (t, i) {
            'undefined' != typeof exports ? module.exports = i : 'function' == typeof define && define.amd ? define([], i) : t.Sticky = i
        }(this, Sticky);

        const menu = $('.c-menu');
        const indicator = $('<span class="c-menu__indicator uk-visible@m"></span>');
        menu.append(indicator);
        position_indicator(menu.find('li.current-menu-item'), indicator);
        setTimeout(function () {
            indicator.css('opacity', 1);
        }, 500);
        menu.find('li').mouseenter(function () {
            position_indicator($(this), indicator);
        });
        menu.find('li').mouseleave(function () {
            position_indicator(menu.find('li.current-menu-item'), indicator);
        });
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};

function position_indicator(ele, indicator) {
    const left = ele.position().left;
    const width = ele.width();
    indicator.stop().animate({
        left: left,
        width: width,
    });
}
