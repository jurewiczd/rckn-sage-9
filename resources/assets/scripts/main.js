// import external dependencies
import 'jquery';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';

// import UIkit
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
});

UIkit.use( Icons );

// Load Events
jQuery(document).ready(() => routes.loadEvents());
