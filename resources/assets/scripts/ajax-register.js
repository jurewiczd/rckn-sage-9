import UIkit from 'uikit';

/* eslint-disable no-undef */
jQuery(document).ready(function ($) {
    // Perform AJAX register on form submit
    $('form#register').on('submit', function (e) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_register.ajaxurl,
            data: {
                'action': 'register',
                'login': $('form#register #login').val(),
                'email': $('form#register #email').val(),
                'password': $('form#register #pass').val(),
                'first_name': $('form#register #first_name').val(),
                'last_name': $('form#register #last_name').val(),
                'security': $('form#register #register_security').val(),
            },
            success: function (data) {
                if (data.registered === true) {
                    UIkit.notification(data.message, {status: 'success'});
                    document.location.href = ajax_register.redirecturl;
                } else {
                    UIkit.notification(data.message, {status: 'danger'})
                }
            },
        });
        e.preventDefault();
    });

});
