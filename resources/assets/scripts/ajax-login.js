import UIkit from 'uikit';

/* eslint-disable no-undef */
jQuery(document).ready(function ($) {
    // Perform AJAX login on form submit
    $('form#login').on('submit', function (e) {
        UIkit.notification(ajax_login.loadingmessage, {status: 'primary'});
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login.ajaxurl,
            data: {
                'action': 'login',
                'username': $('form#login #username').val(),
                'password': $('form#login #password').val(),
                'security': $('form#login #security').val(),
            },
            success: function (data) {
                if (data.loggedin === true) {
                    UIkit.notification(data.message, {status: 'success'});
                    document.location.href = ajax_login.redirecturl;
                } else {
                    UIkit.notification(data.message, {status: 'danger'})
                }
            },
        });
        e.preventDefault();
    });

});
