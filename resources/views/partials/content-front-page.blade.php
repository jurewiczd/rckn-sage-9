<section id="slider" class="c-slider">
    <h2 class="uk-hidden">Slider</h2>
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-2 uk-visible@m"></div>
            <div class="uk-position-relative uk-width-1-2@m" tabindex="-1" uk-slideshow="autoplay: true">
                <ul class="uk-slideshow-items" uk-height-viewport="offset-top: true; offset-bottom: 30">
                    <li class="c-slider__slide uk-active">
                        <div class="uk-position-center-right">
                            <div class="c-slider__content" uk-slideshow-parallax="y: -50,0,0; opacity: 1,1,0">
                                <h2 class="c-slide__heading c-heading c-heading--large uk-margin-remove">Our Advisory &
                                    Decision
                                    Committee</h2>
                                <p class="c-slide__content uk-margin-medium-top">The MBRIF’s Advisory & Decision
                                    Committees (ADC) supports by
                                    providing recommendations to The Ministry of Finance on ligibility, business
                                    potential and market fit of its potential members. </p>
                            </div>
                        </div>
                    </li>
                    <li class="c-slider__slide">
                        <div class="uk-position-center-right">
                            <div class="c-slider__content" uk-slideshow-parallax="y: -50,0,0; opacity: 1,1,0">
                                <h2 class="c-slide__heading c-heading c-heading--large uk-margin-remove">Our Advisory &
                                    Decision
                                    Committee</h2>
                                <p class="c-slide__content uk-margin-medium-top">The MBRIF’s Advisory & Decision
                                    Committees (ADC) supports by
                                    providing recommendations to The Ministry of Finance on ligibility, business
                                    potential and market fit of its potential members. </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="members" class="c-members uk-section">
    <h2 class="uk-hidden">Members</h2>

    <div class="uk-container">
        <h3 class="c-heading c-heading--medium uk-text-center uk-margin-medium-bottom">Showing all {{ count($members) }}
            Committee Members</h3>

        <div class="uk-child-width uk-child-width-1-2@s uk-child-width-1-3@m"
             uk-grid="masonry: true">
            @foreach( $members as $member )
                <div>
                    <div class="c-card uk-card uk-card-default">
                        <div class="c-card__header uk-card-header">
                            <div class="uk-flex uk-flex-between uk-flex-middle">
                                <div class="c-card__icons">
                                    <div class="uk-grid-small uk-child-width-auto uk-flex-left uk-flex-middle" uk-grid>
                                        @if( in_array( 'accelerator', $member->programs ) )
                                            <img width="24" height="25" src="@asset('images/person-rocket.png')">
                                        @endif
                                        @if( in_array( 'guarantee_scheme', $member->programs ) )
                                            <img width="25" height="28" src="@asset('images/person-bush.png')">
                                        @endif
                                    </div>
                                </div>

                                <p class="c-card__category">Education</p>
                            </div>
                        </div>
                        <div class="c-card__body uk-card-body">
                            <div class="uk-grid-small uk-flex-middle uk-margin-bottom" uk-grid>
                                <div class="uk-width-auto">
                                    {!! get_the_post_thumbnail( $member->ID, 'full', ['filter' => true, 'class' => 'uk-border-cirle'] ) !!}
                                </div>
                                <div class="uk-width-expand">
                                    <h3 class="c-heading c-heading--small uk-card-title uk-margin-small-bottom">{{ $member->post_title }}</h3>
                                    <p class="uk-text-meta">{{ $member->position }}</p>
                                </div>
                            </div>
                            <p class="c-card__content">{{ $member->content }}</p>
                        </div>

                        @if( $member->linkedin['url'] )
                            <div class="c-card__footer uk-card-footer">
                                <a href="{{ $member->linkedin['url'] }}" class="c-social">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
                                        <g>
                                            <g>
                                                <path fill="#3377b3"
                                                      d="M10 19v-9H7v9zm3 7C5.82 26 0 20.18 0 13S5.82 0 13 0s13 5.82 13 13-5.82 13-13 13zM8.491 9C9.421 9 10 8.333 10 7.5 9.983 6.647 9.42 6 8.509 6 7.597 6 7 6.648 7 7.5 7 8.332 7.579 9 8.474 9zM20 13.958C20 11.257 18.545 10 16.606 10c-1.565 0-2.267.854-2.657 1.452v-1.245H11c.04.825 0 8.793 0 8.793h2.949v-4.911c0-.262.019-.525.097-.712.213-.526.698-1.07 1.512-1.07 1.067 0 1.494.807 1.494 1.989V19H20z"/>
                                            </g>
                                        </g>
                                    </svg>
                                    <span class="c-social__profile">See profile
                                <svg xmlns="http://www.w3.org/2000/svg" class="c-social__profile__icon" width="9"
                                     height="9"
                                     viewBox="0 0 9 9">
                                    <g>
                                        <g transform="rotate(-45 4.5 4.5)">
                                            <path fill="#0e4fec"
                                                  d="M10.438 4.183a.439.439 0 0 0-.103-.15L5.145-.881a.48.48 0 0 0-.653 0 .42.42 0 0 0 0 .618l4.403 4.17-9.48-.128a.45.45 0 0 0-.461.437.45.45 0 0 0 .461.437l9.48.128-4.403 4.17a.42.42 0 0 0 0 .618c.09.085.208.128.326.128a.474.474 0 0 0 .327-.128l5.19-4.916a.435.435 0 0 0 .1-.143l.003-.006a.42.42 0 0 0 0-.32z"/>
                                        </g>
                                    </g>
                                </svg>
                            </span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
