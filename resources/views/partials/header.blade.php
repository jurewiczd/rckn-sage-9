<div id="offcanvas-push" uk-offcanvas="mode: push; overlay: true">
    <div class="uk-offcanvas-bar uk-flex uk-flex-column uk-flex-center uk-flex-middle">
        <button class="uk-offcanvas-close" type="button" uk-close></button>

        @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'c-navbar uk-nav uk-nav-primary uk-nav-center uk-margin-small', 'li_class' => 'c-navbar__list-item']) !!}
        @endif

        <div class="uk-inline uk-margin-small">
            @if(!is_user_logged_in())
                <button class="c-header__login-btn uk-button uk-button-default uk-flex uk-flex-middle"
                        type="button">Login
                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="5" viewBox="0 0 9 5"
                         class="c-header__login-btn__icon uk-margin-small-left">
                        <g transform="rotate(-180 4.5 3)">
                            <path fill="#fff" d="M4.5 1L8 4.667H1z"/>
                            <path fill="none" stroke="#fff" stroke-linejoin="round" stroke-miterlimit="50"
                                  d="M4.5 1v0L8 4.667v0H1v0z"/>
                        </g>
                    </svg>
                </button>
                <div class="c-login-dropdown" uk-dropdown="mode: click; pos: bottom-justify;">
                    <form id="login">
                        <div class="uk-margin-bottom">
                            <div class="uk-inline">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                                <input class="uk-input" type="text" id="username" name="username">
                            </div>
                        </div>

                        <div class="uk-margin-bottom">
                            <div class="uk-inline">
                                <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                                <input class="uk-input" type="password" id="password" name="password">
                            </div>
                        </div>

                        @php( wp_nonce_field( 'ajax-login-nonce', 'security' ) )

                        <div>
                            <button class="uk-button uk-button-default uk-width-1-1">Login</button>
                        </div>
                    </form>
                </div>
            @else
                <a class="c-header__login-btn uk-button uk-button-default uk-flex uk-flex-middle"
                   href="{{ wp_logout_url( home_url()) }}">Logout
                </a>
            @endif
        </div>

        <a class="c-header__apply-btn uk-button uk-button-default uk-margin-small" href="{{ get_permalink(28) }}">Apply Now</a>
    </div>
</div>

<header class="c-header cd-auto-hide-header {{ is_page(['users', 'register']) ? 'c-header--is-scrolled' : '' }}">
    <nav
        class="uk-navbar-container uk-container uk-container-large uk-container-expand-right uk-navbar-transparent uk-navbar uk-padding uk-padding-remove-top uk-padding-remove-bottom uk-flex-center uk-flex-middle">
        <div class="uk-navbar-left">
            <a class="uk-navbar-item uk-logo" href="{{ home_url('/') }}">
                <img data-src="@asset('images/svg/logo.svg')" alt="Logo" uk-img>
            </a>
        </div>

        <div class="uk-navbar-right">
            @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'container_class' => 'c-menu', 'menu_class' => 'c-navbar uk-navbar-nav uk-visible@m', 'li_class' => 'c-navbar__list-item']) !!}
            @endif

            <div class="uk-navbar-item uk-margin-large-left@l uk-visible@xl">
                <div class="uk-inline">
                    @if(!is_user_logged_in())
                        <button class="c-header__login-btn uk-button uk-button-default uk-flex uk-flex-middle"
                                type="button">Login
                            <svg xmlns="http://www.w3.org/2000/svg" width="9" height="5" viewBox="0 0 9 5"
                                 class="c-header__login-btn__icon uk-margin-small-left">
                                <g transform="rotate(-180 4.5 3)">
                                    <path fill="#fff" d="M4.5 1L8 4.667H1z"/>
                                    <path fill="none" stroke="#fff" stroke-linejoin="round" stroke-miterlimit="50"
                                          d="M4.5 1v0L8 4.667v0H1v0z"/>
                                </g>
                            </svg>
                        </button>
                        <div class="c-login-dropdown" uk-dropdown="mode: click; pos: bottom-justify;">
                            <form id="login">
                                <div class="uk-margin-bottom">
                                    <div class="uk-inline">
                                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                                        <input class="uk-input" type="text" id="username" name="username">
                                    </div>
                                </div>

                                <div class="uk-margin-bottom">
                                    <div class="uk-inline">
                                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                                        <input class="uk-input" type="password" id="password" name="password">
                                    </div>
                                </div>

                                @php( wp_nonce_field( 'ajax-login-nonce', 'security' ) )

                                <div>
                                    <button class="uk-button uk-button-default uk-width-1-1">Login</button>
                                </div>
                            </form>
                        </div>
                    @else
                        <a class="c-header__login-btn uk-button uk-button-default uk-flex uk-flex-middle"
                           href="{{ wp_logout_url( home_url()) }}">Logout
                        </a>
                    @endif
                </div>
            </div>

            <div class="uk-navbar-item uk-visible@xl">
                <a class="c-header__apply-btn uk-button uk-button-default" href="{{ get_permalink(28) }}">Apply Now</a>
            </div>

            <div class="uk-navbar-item uk-visible@l uk-margin-medium-left@l uk-padding-remove">
                <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="14" viewBox="0 0 21 14">
                        <g>
                            <path fill="#fff"
                                  d="M10.386 14.002C4.726 14.002.3 7.712.114 7.444a.634.634 0 0 1 0-.726C.3 6.45 4.726.16 10.386.16s10.086 6.29 10.271 6.558a.634.634 0 0 1 0 .726c-.185.268-4.611 6.558-10.271 6.558m0-12.53c-4.214 0-7.86 4.313-8.86 5.608 1.002 1.296 4.655 5.61 8.86 5.61 4.213 0 7.86-4.313 8.86-5.609-1.002-1.296-4.655-5.609-8.86-5.609"/>
                        </g>
                        <g>
                            <path fill="#fff"
                                  d="M10.5 11C8.57 11 7 9.43 7 7.5S8.57 4 10.5 4 14 5.57 14 7.5 12.43 11 10.5 11m.005-5.645c-1.185 0-2.15.964-2.15 2.15s.965 2.15 2.15 2.15c1.186 0 2.15-.964 2.15-2.15s-.964-2.15-2.15-2.15"/>
                        </g>
                    </svg>
                </a>
            </div>

            <div class="uk-navbar-item">
                <ul class="uk-nav uk-nav-default">
                    <li>
                        <a href="#0"
                           class="c-header__lang-switch c-header__lang-switch--padding c-header__lang-switch--is-active">EN</a>
                    </li>

                    <li>
                        <a href="#0" class="c-header__lang-switch c-header__lang-switch--padding">AR</a>
                    </li>
                </ul>
            </div>

            <a class="uk-navbar-toggle uk-hidden@m" uk-navbar-toggle-icon uk-toggle href="#offcanvas-push"></a>
        </div>
    </nav>
</header>
