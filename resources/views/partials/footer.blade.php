<section id="footer" class="c-footer uk-section">
    <h2 class="uk-hidden">Footer</h2>
    <div class="uk-container uk-container-large">
        <div class="uk-child-width-1-3@m uk-child-width-1-4@l" uk-grid>
            <div class="c-footer__column">
                <h4 class="c-footer__heading c-heading--small uk-margin-bottom">Sitemap</h4>

                <div class="c-footer__content uk-column-1-2@s">
                    <a href="#0" class="c-footer__link uk-display-block">Home page</a>
                    <a href="#0" class="c-footer__link uk-display-block">About us</a>
                    <a href="#0" class="c-footer__link uk-display-block">A&D Committee</a>
                    <a href="#0" class="c-footer__link uk-display-block">Accelerator</a>
                    <a href="#0" class="c-footer__link uk-display-block">Experts</a>
                    <a href="#0" class="c-footer__link uk-display-block">Guarantee scheme</a>
                    <a href="#0" class="c-footer__link uk-display-block">Members</a>
                    <a href="#0" class="c-footer__link uk-display-block">Media Center</a>
                    <a href="#0" class="c-footer__link uk-display-block">Contact us</a>
                    <a href="#0" class="c-footer__link uk-display-block">Apply now</a>
                    <a href="#0" class="c-footer__link uk-display-block">FAQ</a>
                </div>
            </div>

            <div class="c-footer__column">
                <h4 class="c-footer__heading c-heading--small uk-margin-bottom">Contact</h4>

                <div class="c-footer__content">
                    <p class="c-footer__text">Abu Dhabi: P.O.Box 51515, Abu Dhabi, UAE</p>
                    <p class="c-footer__text">Dubai: P.O.Box 34567, Dubai, UAE</p>
                    <p class="c-footer__text uk-margin-small-top">800-27-274</p>
                </div>
            </div>

            <div class="c-footer__column">
                <h4 class="c-footer__heading c-heading--small uk-margin-bottom">Newsletter</h4>

                <div class="c-newsletter">
                    <div class="uk-inline uk-width-1-2@s uk-width-1-1@m">
                        <form>
                            <input class="c-newsletter__input uk-input" type="email" name="email" id="newsletter-mail"
                                   placeholder="ENTER E-MAIL">
                            <button class="c-newsletter__button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="13" viewBox="0 0 17 13">
                                    <g>
                                        <path fill="#fff"
                                              d="M17 6.5a.525.525 0 0 0-.038-.194l-.003-.008a.528.528 0 0 0-.119-.174L10.738.155a.552.552 0 0 0-.768 0 .523.523 0 0 0 0 .75l5.178 5.064H.543c-.3 0-.543.238-.543.531 0 .293.243.53.543.53h14.605L9.97 12.095a.523.523 0 0 0 0 .75.546.546 0 0 0 .384.156c.139 0 .278-.052.384-.155l6.102-5.97a.528.528 0 0 0 .12-.173A.525.525 0 0 0 17 6.5"/>
                                    </g>
                                </svg>
                            </button>
                        </form>
                    </div>
                </div>

                <h4 class="c-footer__heading c-heading--small uk-margin-medium-top uk-margin-bottom">Social media</h4>

                <div
                    class="uk-grid-small uk-child-width-1-6 uk-child-width-1-5@m uk-child-width-1-6@l uk-flex-left uk-text-center"
                    uk-grid>
                    <a href="#0" class="c-social">
                        <img src="@asset('images/svg/footer-linkedin.svg')" alt="LinkedIn">
                    </a>
                    <a href="#0" class="c-social">
                        <img src="@asset('images/svg/footer-twitter.svg')" alt="Twitter">
                    </a>
                    <a href="#0" class="c-social">
                        <img src="@asset('images/svg/footer-instagram.svg')" alt="Instagram">
                    </a>
                    <a href="#0" class="c-social">
                        <img src="@asset('images/svg/footer-facebook.svg')" alt="Facebook">
                    </a>
                    <a href="#0" class="c-social">
                        <img src="@asset('images/svg/footer-youtube.svg')" alt="YouTube">
                    </a>
                </div>
            </div>

            <div class="c-footer__column uk-width-1-1 uk-width-1-4@l uk-text-center uk-text-left@l">
                <div class="c-footer__content c-footer__company">
                    <svg xmlns="http://www.w3.org/2000/svg" width="66" height="65" viewBox="0 0 66 65"
                         class="c-footer__company__logo">
                        <g>
                            <g>
                                <path fill="#fff"
                                      d="M4.42 4.248l38.207-.01v37.456l-38.206.013zM47.049-.1L0-.085v46.128l47.048-.015z"/>
                            </g>
                            <g>
                                <path fill="#fff"
                                      d="M23.42 22.348l38.207-.013v37.461l-38.208.01zM19 18.015v46.128l47.048-.015V18z"/>
                            </g>
                        </g>
                    </svg>

                    <p class="c-footer__text uk-margin-medium-top">Mohammed bin Rashid Innovation Fund
                        © {{ date('Y') }}</p>
                </div>
            </div>
        </div>
    </div>
</section>
