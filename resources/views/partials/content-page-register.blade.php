<section id="register" class="uk-margin-xlarge-top uk-margin-large-bottom">
    <div class="uk-container">
        <form id="register">
            <p class="status"></p>
            <div class="uk-margin-bottom">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                    <input class="uk-input" type="text" id="login" name="login" placeholder="Login">
                </div>
            </div>

            <div class="uk-margin-bottom">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: mail"></span>
                    <input class="uk-input" type="email" id="email" name="email" placeholder="E-mail">
                </div>
            </div>

            <div class="uk-margin-bottom">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                    <input class="uk-input" type="password" id="pass" name="pass" placeholder="Password">
                </div>
            </div>

            <div class="uk-margin-bottom">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                    <input class="uk-input" type="text" id="first_name" name="first_name" placeholder="First name">
                </div>
            </div>

            <div class="uk-margin-bottom">
                <div class="uk-inline uk-width-1-1">
                    <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                    <input class="uk-input" type="text" id="last_name" name="last_name" placeholder="Last name">
                </div>
            </div>

            @php( wp_nonce_field( 'ajax-register-nonce', 'register_security' ) )

            <div>
                <button class="uk-button uk-button-default uk-width-1-1">register</button>
            </div>
        </form>
    </div>
</section>
