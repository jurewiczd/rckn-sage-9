<section id="users" class="c-user-list uk-margin-xlarge-top uk-margin-large-bottom">
    <div class="uk-container">
        <table class="uk-table uk-table-divider">
            <thead>
            <tr>
                <th>Login</th>
                <th>Display name</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->data->user_login }}</td>
                    <td>{{ $user->data->display_name }}</td>
                    <td>{{ $user->data->user_email }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</section>
