<?php

namespace App;

use App\PostTypes\Person;

/**
 * Define custom fields
 * Docs: https://carbonfields.net/docs/
 */
add_action( 'init', function () {
    Person::register();
} );
