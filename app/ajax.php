<?php

class Ajax {
    public function __construct() {
        add_action( 'wp_ajax_nopriv_login', [ $this, 'login' ] );
        add_action( 'wp_ajax_nopriv_register', [ $this, 'register' ] );
    }

    public function login() {
        // First check the nonce, if it fails the function will break
        check_ajax_referer( 'ajax-login-nonce', 'security' );

        // Nonce is checked, get the POST data and sign user on
        $info                  = [];
        $info['user_login']    = $_POST['username'];
        $info['user_password'] = $_POST['password'];
        $info['remember']      = true;

        $user_signon = wp_signon( $info, false );
        if ( is_wp_error( $user_signon ) ) {
            echo json_encode( [ 'loggedin' => false, 'message' => __( 'Wrong username or password.' ) ] );
        } else {
            echo json_encode( [ 'loggedin' => true, 'message' => __( 'Login successful, redirecting...' ) ] );
        }

        die();
    }

    public function register() {
        check_ajax_referer( 'ajax-register-nonce', 'security' );

        $login      = stripcslashes( $_POST['login'] );
        $email      = stripcslashes( $_POST['email'] );
        $first_name = stripcslashes( $_POST['first_name'] );
        $last_name  = stripcslashes( $_POST['last_name'] );
        $password   = $_POST['password'];
        $user_data  = [
            'user_login'    => $login,
            'user_email'    => $email,
            'user_pass'     => $password,
            'user_nicename' => $login,
            'display_name'  => "{$first_name}  {$last_name}",
            'first_name'    => $first_name,
            'last_name'     => $last_name,
            'role'          => 'subscriber'
        ];
        $user_id    = wp_insert_user( $user_data );

        if ( ! is_wp_error( $user_id ) ) {
            echo json_encode( [ 'registered' => true, 'message' => __( 'Your account has been created.' ) ] );
        } else {
            echo json_encode( [ 'registered' => false, 'message' => $user_id->get_error_message() ] );
        }

        die();
    }
}

new Ajax();
