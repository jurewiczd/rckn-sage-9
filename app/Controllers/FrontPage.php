<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller {
    public function members() {
        $args = [
            'post_type'      => 'person',
            'posts_per_page' => - 1,
        ];

        $members = ( new \WP_Query( $args ) )->posts;

        foreach ( $members as &$member ) {
            $member->position = carbon_get_post_meta( $member->ID, 'position' );
            $member->content  = carbon_get_post_meta( $member->ID, 'content' );
            $member->programs = carbon_get_post_meta( $member->ID, 'programs' );
            $member->linkedin = carbon_get_post_meta( $member->ID, 'linkedin' );
        }

        return $members;
    }
}
