<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageUsers extends Controller {
    public function users() {
        return get_users();
    }
}
