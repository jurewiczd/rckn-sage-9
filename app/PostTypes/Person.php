<?php

namespace App\PostTypes;

class Person {
    public static function register() {
        register_post_type(
            'person',
            [
                'labels'              => [
                    'name'              => __( 'Pracownik', 'rockon' ),
                    'singular_name'     => __( 'Pracownik', 'rockon' ),
                    'all_items'         => __( 'Wszyscy pracownicy', 'rockon' ),
                    'add_new'           => __( 'Dodaj pracownika', 'rockon' ),
                    'add_new_item'      => __( 'Dodaj nowego pracownika', 'rockon' ),
                    'edit'              => __( 'Edytuj', 'rockon' ),
                    'edit_item'         => __( 'Edytuj pracownika', 'rockon' ),
                    'new_item'          => __( 'Nowy pracownik', 'rockon' ),
                    'parent_item_colon' => '',
                ],
                'public'              => true,
                'publicly_queryable'  => false,
                'exclude_from_search' => false,
                'show_ui'             => true,
                'query_var'           => true,
                'menu_position'       => 8,
                'menu_icon'           => 'dashicons-hammer',
                'has_archive'         => false,
                'capability_type'     => 'post',
                'hierarchical'        => true,
                'supports'            => [ 'title', 'editor', 'thumbnail' ],
            ]
        );

    }
}
