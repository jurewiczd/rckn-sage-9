<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Person {
    public static function generate() {
        Container::make( 'post_meta', 'Członek zespołu' )
                 ->where( 'post_type', '=', 'person' )
                 ->add_fields( [
                     Field::make( 'text', 'position', 'Stanowisko' )
                          ->set_required( true ),
                     Field::make( 'textarea', 'content', 'Opis' )
                          ->set_required( true ),
                     Field::make( 'multiselect', 'programs', 'Program' )
                          ->add_options( array(
                              'accelerator'      => 'Accelerator',
                              'guarantee_scheme' => 'Guarantee Scheme',
                          ) ),
                     Field::make( 'urlpicker', 'linkedin', 'LinkedIn' ),
                 ] );
    }
}
